<?php

require '../../vendor/autoload.php';

use Ltt\Rc4Code;

$rc4 = new Rc4Code();

$str = 'aaaaaaaaaa';
echo '原字符串：'.$str.'</br>';
$encodeRc4 = $rc4->encode($str);
echo '加密字符串：'.$encodeRc4.'</br>';

$decodeRc4 = $rc4->decode($encodeRc4);
echo '解密字符串：'.$decodeRc4.'</br>';exit;